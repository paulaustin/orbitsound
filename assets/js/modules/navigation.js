/*
    Navigation
    ----------

    Implements visibility toggle for responsive navigation menu.
*/
define(function(config, method){

    config = {};

    function Init(){

        method = this;

        config.nav = $(".site-navigation ul");
        config.tog = $('<span class="toggle" />').insertBefore(config.nav);

        config.tog.on("click", function(){

            config.nav.toggleClass("is-visible");

        });

    }

    return Init();

});