/*
    Global Utility functions
    ----------------

    Sets of common utility functions for
    use on this project.
*/
var utility = {
    require_if: function(element, requirement){
        return ($(element).length) ? require([requirement[0]], requirement[1]) : false;
    }
};