﻿Title: T9 Black Soundbar

----

Description: ## Description

At just 30cm in length the T9 soundbar is similar in size to a centre speaker of a 5.1 system.  Designed to sit comfortably in front of smaller TVs or on its own and with 140W RMS of power, the T9 consistently delivers great sound.  As a result of Orbitsound’s spatial stereo technology, audio is carried without interference throughout the room, creating an infinite sweet spot. And, unlike complex 5.1 systems with numerous speakers and wires, the neat T9 is made up of the unit itself, and a small subwoofer.  Orbitsound has redesigned the subwoofer for the T9, to compliment the tonal character and smaller dimensions of the soundbar. 

The T9 features an integrated charger so users can power-up their iPhone or iPod whilst watching TV or listening to music.  Its remote control gives users access to the full iMenu, as well as controlling the T9’s volume, bass and treble settings.  Made from lacquered wood, the T9 cabinet is almost completely non-resonant, delivering a pure and clean sound whether running at high or low volume; and free from buzzes, rattles or resonances that would spoil your music or speech audio.  The T9 is finished with a magnetically attached damped metal front grill and comes complete with a full set of cables; including optical, AUX, speaker, AUX in and VID out phono cable for the ever popular video out function which allows users to watch video content (eg YouTube or BBC iPlayer) from their iPod or iPhone on their TV screen via the T9. 

 With its practical nature, the T9 is likely to appeal to a variety of audiences; including those looking to improve their TV audio from smaller screens or TVs in secondary rooms, and also for consumers who want an iPod dock that delivers powerful auditorium quality spatial stereo sound that really fills the room but at a highly competitive price.  Gamers will also benefit from the small footprint of the soundbar, perfect for bedroom games consoles and PC set ups. The T9 produces a realistic landscape of sound that is highly dynamic.  Multiplayer gamers will feel immersed in the vibrant range, creating convincing atmosphere and audio for any game situation.

----

Features: ## Features

### Sound Bar
- Dimensions: 300x94x94mm
- Speakers: 2x2.5" mid-high drivers, 1x1" tweeter, 2x1" midrange spatial generators
- Enclosure: Single enclosed infinite baffle arrangement
- Power: 140W RMS.  80W Sub, 35W Main, 25W Spatial Crossover Frequencies: 160Hz/5KHz

### Sub woofer
- Dimensions: 230x139x344mm
- Type: Tuned ported enclosure
- Speakers: 1x6.5" long throw subwoofe

----

Reviews: 

----

Videos: 