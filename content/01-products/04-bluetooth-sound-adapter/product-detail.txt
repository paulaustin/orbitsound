﻿Title: Bluetooth Sound Adapter

----

Description: ## Description

Turn your Orbitsound soundbar with dock into a wireless music station for all your Bluetooth enabled devices. Simply dock the adaptor, pair your device, and enjoy digital streamed sound wirelessly to your soundbar.

Using A2DP high fidelity stereo, this adaptor delivers high quality streaming with the flexibility of wireless freedom, placing control completely in your hands. 
 
### Instructions

1. Dock the adaptor on your soundbar. (You will see the light on the soundbar flash red for 4-5 seconds. The light will then turn blue.)
2. Look for ‘Soundbar’ Bluetooth device on your Phone/Tablet/Laptop and tap ‘Pair’
3. If asked for a PIN, enter ‘0000’ and enter/pair.

\*To connect to a different device, un-pair a connected device, and complete steps 2&3 on new device.

_Please note, not compatible with Orbitsound T12v1 or T12v2. Compatible with T12v3, T9 and orbitsoundbar (see rear of soundbar to check the version). This adaptor is not guaranteed to work on docks by other manufacturers._

----

Features: 

----

Reviews: 

----

Videos: 