# Compass configuration file.
require 'C:/Ruby193/lib/ruby/gems/1.9.1/gems/susy-1.0.8/lib/susy.rb'

# We also support plugins and frameworks, please read the docs http://docs.mixture.io/preprocessors#compass

project_path = "/xampp/htdocs/orbitsound"

# Important! change the paths below to match your project setup
css_dir = "assets/css" # update to the path of your css files.
sass_dir = "assets/scss" # update to the path of your sass files.
images_dir = "assets/img" # update to the path of your image files.
javascripts_dir = "assets/js" # update to the path of your script files.

line_comments = false # if debugging (using chrome extension - set this to true)
cache = true
color_output = false # required for Mixture