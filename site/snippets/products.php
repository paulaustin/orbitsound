<ul class="product-block">

    <?php foreach($data->children()->visible() as $project): ?>
    <li>
        <figure>
            <a href="<?php echo $project->url() ?>"><img src="<?php echo $project->images()->first()->url() ?>" alt="<?php echo html($project->title()) ?>" /></a>
        </figure>
    </li>
    <?php endforeach ?>

</ul>