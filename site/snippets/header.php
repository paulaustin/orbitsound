<!DOCTYPE html>

<!--[if lt IE 7]><html lang="en" class="no-js lt-ie7 ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js lt-ie9 ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js lt-ie10 ie9"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en" class="no-js"><!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="index, follow" name="robots">

    <title><?php echo html($site->title()) ?> - <?php echo html($page->title()) ?></title>

    <meta name="description" content="<?php echo html($site->description()) ?>" />
    <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
    <meta content="Fantastic Thinking" http-equiv="Author" name="author">

    <!-- Stylesheets -->
    <?php echo css('assets/css/screen.css') ?>

    <!--[if lt IE 9]>
        <script src="/static/assets/js/libs/html5.min.js"></script>
        <script src="/static/assets/js/libs/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and iOS Touch Icons -->
    <link href="/static/assets/ico/favicon.png" type="image/png" rel="shortcut icon">
    <link href="/static/assets/ico/apple-touch-icon-114-precomposed.png" sizes="114x114" rel="apple-touch-icon-precomposed">
    <link href="/static/assets/ico/apple-touch-icon-72-precomposed.png" sizes="72x72" rel="apple-touch-icon-precomposed">
    <link href="/static/assets/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">

</head>

<body>

    <div class="page">

        <header class="header">
            <div><a href="<?php echo url() ?>"><img src="<?php echo url('assets/img/orbitsound_logo.gif') ?>" alt="<?php echo html($site->title()) ?>" /></a></div>
        </header>