<?php snippet('header') ?>

<?php snippet('menu') ?>

<div class="content">

    <div class="main">

        <h1><?php echo html($page->title()) ?></h1>
        <?php echo kirbytext($page->description()) ?>
        <?php echo kirbytext($page->features()) ?>
        <?php echo kirbytext($page->reviews()) ?>
        <?php echo kirbytext($page->videos()) ?>
    </div>

    <?php snippet('submenu') ?>

</div>

<?php snippet('footer') ?>