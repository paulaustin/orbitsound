<?php snippet('header') ?>

<?php snippet('menu') ?>

<div class="content landing-page">

    <div class="main">

        <h1><?php echo html($page->title()) ?></h1>
        <?php echo kirbytext($page->text()) ?>

        <?php foreach($pages->visible() as $section): ?>
            <?php snippet($section->uid(), array('data' => $section)) ?>
        <?php endforeach ?>

    </div>

</div>

<?php snippet('footer') ?>