<?php snippet('header') ?>

<?php snippet('menu') ?>

<div class="content">

    <div class="main">
        <h1><?php echo html($page->title()) ?></h1>
        <?php echo kirbytext($page->text()) ?>
    </div>

    <?php snippet('submenu') ?>

</div>

<?php snippet('footer') ?>